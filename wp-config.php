<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jactest_db');

/** MySQL database username */
define('DB_USER', 'jactest_usr');

/** MySQL database password */
define('DB_PASSWORD', 'RnN2y508xP3XMfJjGnuaN7TSCHBMdn0PxviIcrw2kwODoluwMSGlptSrgr07WtQ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cp81ei7]6W%?EWY-&TW7x3ZXklygYA=j_Y#^s)FTkz}Xp?}LXO!:f4=9y[^OoB>');
define('SECURE_AUTH_KEY',  '83]eKs*hJ3lH~n_Yv,N37ON!|h,[bJt~Z^-F>.<MR|Me1%~j/rZW93QkCJF/C1~');
define('LOGGED_IN_KEY',    'q&y:Ts!&BR@JBtv(!T^>Uq0Aq?o(khOY<"-#!#lc;LIIH8w_v%F+pq:M]B!X8b!');
define('NONCE_KEY',        '|RC/Y}V7ex?zBxG2gSG6VTDx|Kfqb1}.-$Fr:Sd<#&r-Z8t\^7g"3u\<anpT!j-');
define('AUTH_SALT',        ':T/Rlrs(`-p*T<_)U>J-"DSke%r4=B(YJ$+wI"76dCs3NS%)^Fz/5$|F^m?IY-z');
define('SECURE_AUTH_SALT', 'vF$5W`F2)Efq!Q/;nCcqokzILzOH.U0,4k@]48E<\$qN@Ev_;<:sxbz`VJEx=j.');
define('LOGGED_IN_SALT',   ';FiYO9m,0A*dLn"(py#^q0U9S+]PHw}~K%O?b=K4"dQ.R!x]I^IIRv,c5.vVj7[');
define('NONCE_SALT',       'YpLv)W3o.S.5^Y7$_tRYJ_*b~#X<ceTx}0DPAPHkCn(5L9Po4|YM"3N;RvIe#cx');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'jacwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
