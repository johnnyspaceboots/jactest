<?php

/**
 * Get child theme directory
 *
 * I'm sure Wordpress has a way of doing this. I'm also sure I don't know what
 * it is right now.
 *
 * @TODO Figure out how to get child theme directory correctly
 * @return [String] path to child theme directory
 */
function _get_theme_directory() {

  return get_template_directory_uri() . '/../jactest/';
}

/**
 * Load CSS necessary for theme
 *
 * @TODO Move script queuing to its own function
 */
function theme_enqueue_styles() {

    $parent_style = 'parent-style';
    $child_style = 'child-style';
    $jquery_script = 'jquery-script';
    $theme_directory = _get_theme_directory();

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( $child_style,
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
    wp_enqueue_style( 'slick-style',
        $theme_directory . 'bower_components/slick-carousel/slick/slick.css',
        array($child_style)
    );
    wp_enqueue_style( 'slick-theme-style',
        $theme_directory . 'bower_components/slick-carousel/slick/slick-theme.css',
        array($child_style)
    );

    wp_enqueue_script($jquery_script,
      $theme_directory . 'bower_components/jquery/dist/jquery.min.js'
    );
    wp_enqueue_script('slick-script',
      $theme_directory . 'bower_components/slick-carousel/slick/slick.min.js',
      array($jquery_script)
    );
}

/**
 * Create Feature custom post type
 */
function create_post_type() {

  $labels = array(
    'name' => _x('Features', ''),
    'singular_name' => _x('Feature', '')
  );

  $args = array(
    'labels' => $labels,
    'description' => 'Test Desc',
    'public' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'supports' => array('title', 'editor', 'custom-fields')
  );

  register_post_type( 'features', $args);
}

/**
 * Get custom fields for a given feature type post
 */
function get_feature_metadata($id) {

  return (object)[
    'Pitches' => _get_sequential_custom_fields($id, 'Pitch'),
    'Blocks'  => _get_sequential_custom_fields($id, 'Block',  ['Title', '', 'Image', 'Action']),
    'Footer'  => _get_sequential_custom_fields($id, 'Footer', ['Title', '', 'Image', 'Action'])
  ];
}

/**
 * Get a sequential series of customs fields for a given postId
 *
 * Retreives all numbered and sequential custom fields with a given prefix and
 * collection of suffixes. Will return an array of objects.
 *
 * @param [Integer] $postId
 * @param [String] $fieldPrefix
 * @param [Array of String] $fieldSuffixes
 * @return [Array of Object]
 */
function _get_sequential_custom_fields($postId, $fieldPrefix, $fieldSuffixes = ['Title', '', 'Image']) {

  $loop = true;
  $count = 1;
  $fields = [];

  while ($loop) {
    $meta = [];

    foreach ($fieldSuffixes as $suffix) {
      $meta[$suffix ? $suffix : 'Content'] = get_post_meta($postId, trim($fieldPrefix . ' ' . $count . ' ' . $suffix), true);

      if (!$meta[$suffix ? $suffix : 'Content']) {
        $loop = false;
      }
    }

    if ($loop) {
      $fields[] = (object)$meta;
      $count++;
    }
  }

  return $fields;
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
add_action( 'init', 'create_post_type' );
?>
