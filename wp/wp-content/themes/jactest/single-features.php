<?php
  $feature = get_feature_metadata($post->ID);
?>

<div class='carousel-controls' data-carousel-id="pitch-carousel">
  <?php $x = 1; foreach($feature->Pitches as $pitch): ?>
    <div class="<?= $x == 1 ? 'active' : '' ?>">
      <h1><?= $pitch->Title ?></h1>
      <p><?= $pitch->Content ?></p>
    </div>
  <?php $x++; endforeach; ?>

  <ul>
    <?php $x = 1; foreach($feature->Pitches as $pitch): ?>

      <?php if($x == 1): ?>
        <li class="active"><img src="<?= _get_theme_directory() ?>img/bullet-selected.png" /></li>
      <?php else: ?>
        <li><img src="<?= _get_theme_directory() ?>img/bullet-unselected.png" /></li>
      <?php endif; ?>

    <?php $x++; endforeach; ?>
  </ul>

  <div class="sidebar" id="hero-sidebar"></div>

  <div id="hero-action"><span>view</span></div>
</div>

<div class='carousel' id="pitch-carousel">
  <?php foreach($feature->Pitches as $pitch): ?>
    <div style="background: url('<?= $pitch->Image ?>');">

    </div>
  <?php endforeach; ?>
</div>

<div class="viewport-spacer"></div>

<?php $x = 1; foreach($feature->Blocks as $block): ?>
  <div class='row block'>
    <?php if($x % 2): ?>
      <div class='col-xs-6'>
        <div class='spacer'>
          <h1><?= $block->Title ?></h1>
          <p><?= $block->Content ?></p>
          <button><?= $block->Action ?></button>
        </div>
      </div>

      <div class='col-xs-6 img-block' style="background: url('<?= $block->Image ?>');">&nbsp;</div>
    <?php else: ?>
      <div class='col-xs-6 img-block right' style="background: url('<?= $block->Image ?>');">&nbsp;</div>

      <div class='col-xs-6 col-xs-offset-6'>
        <div class="spacer right">
          <h1><?= $block->Title ?></h1>
          <p><?= $block->Content ?></p>
          <button><?= $block->Action ?></button>
        </div>
      </div>
    <?php endif; ?>

  </div>
<?php $x++; endforeach; ?>

<div class='row footer' style="background: url('<?= $feature->Footer[0]->Image ?>')">
  <div class='col-xs-4'>
    <h1><?= $feature->Footer[0]->Title ?></h1>
    <p><?= $feature->Footer[0]->Content ?></p>
    <button><?= $feature->Footer[0]->Action ?></button>
  </div>
</div>

<script>
(function() {

  $(document).ready(function() {
    $('.carousel').slick({
      autoplay: false
    });
  });
}());

</script>

<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
