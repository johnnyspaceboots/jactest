<footer>
<div class="row" id='page-footer'>
  <div class="col-xs-3">
    <img src="<?= _get_theme_directory() ?>img/footer-logo.png" />
  </div>
  <div class="col-xs-9">
    <ul>
      <li>FB</li>
      <li>SITEMAP</li>
      <li>LEGAL</li>
      <li>ABOUT US</li>
    </ul>
  </div>
</div>

<div class="row" id="bottom-footer">
  <div class="col-xs-4">
    <p>Copyright 2015 Emberley</p>
  </div>
  <div class="col-xs-4">
    71 Sherose Island Lane | Mount Pearl | A1E 2E3
  </div>
  <div class="col-xs-4">
    Design & Tech
  </div>
</div>
</footer>
