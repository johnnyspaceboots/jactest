<!-- TODO: Load this correctly / move to where appropriate -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600,800|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

<header class="row" id="header">
  <div id="logo" class="col-xs-3">
    <img src="<?= _get_theme_directory() ?>img/logo.png" />
    <div class="sidebar" id="logo-sidebar"></div>
  </div>

  <nav id="navigation" role="navigation" class="col-xs-9">
    <div class="row" id="top-nav">
      <ul>
        <!-- Reversed order to compensate for float: right; -->
        <li>tw</li>
        <li>fb</li>
        <li>contact</li>
        <li>help center</li>
        <li>the latest</li>
      </ul>
    </div>

    <div class="row" id="bottom-nav">
      <ul>
        <!-- Reversed order to compensate for float: right; -->
        <li>outdoors</li>
        <li>inserts</li>
        <li>fireplaces</li>
        <li>stoves</li>
        <li>trends</li>
      </ul>
    </div>
  </nav>
</header>
